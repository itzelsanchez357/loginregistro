package com.example.activitylogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityPerfil extends AppCompatActivity {
    List<Usuario> list = new ArrayList<Usuario>();
    Usuario usuario1;
    Registro registro;
    private EditText EdtNumero;
    private Button btnNumero;
    private final int PHONE_CALL_CODE = 100;
    EditText edtNombre;
    EditText edtEdad;
    EditText edtDescripcion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        String usuario_extra = getIntent().getExtras().getString("usuario_registro_extra_l");
        String nombre_extra = getIntent().getExtras().getString("nombre_registro_extra_l");
        String edad_extra = getIntent().getExtras().getString("edad_registro_extra_l");
        String telefono_extra = getIntent().getExtras().getString("telefono_registro_extra_l");
        String descripcion_extra = getIntent().getExtras().getString("descripcion_registro_extra_l");
        String password_extra = getIntent().getExtras().getString("password_registro_extra_l");

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtNombre.setText(nombre_extra);

        edtEdad = (EditText) findViewById(R.id.edtEdad);
        edtEdad.setText(edad_extra);

        EdtNumero = (EditText) findViewById(R.id.EdtNumero);
        EdtNumero.setText(telefono_extra);

        edtDescripcion = (EditText) findViewById(R.id.edtDescripcion);
        edtDescripcion.setText(descripcion_extra);

        EdtNumero = (EditText) findViewById(R.id.EdtNumero);
        EdtNumero.setText(telefono_extra);
        btnNumero = (Button) findViewById(R.id.btnNumero);

        Button btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);
            }
        });

        usuario1 = new Usuario(nombre_extra,edad_extra,telefono_extra,descripcion_extra);
        list.add(usuario1);
        registro = new Registro(list);
        for(Usuario datos : registro.getUsuario()){
            btnNumero.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String num = EdtNumero.getText().toString();
                    if(num !=null){
                        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                        }else{
                            versionesAnteriores(num);
                        }
                    }
                }

                private void versionesAnteriores (String num){
                    Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                    if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                        startActivity(intentLlamada);
                    }else{
                        Toast.makeText(ActivityPerfil.this, "configura los permisos", Toast.LENGTH_SHORT).show();
                    }
                }


            });

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if(permission.equals(Manifest.permission.CALL_PHONE)) {

                    if (result == PackageManager.PERMISSION_GRANTED) {
                        String phoneNumber = EdtNumero.getText().toString();
                        // Obtener numero String phoneNumber = ;
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    } else {
                        Toast.makeText(this, "No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean verificarPermisos (String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }


}