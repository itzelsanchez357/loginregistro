package com.example.activitylogin;

import java.util.List;

public class Registro {
    private List<Usuario> usuario;

    public Registro(List<Usuario> usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }
}
